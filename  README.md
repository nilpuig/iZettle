# E-commerce Framework for iZettle



## Set up the framework

1. Download or clone the framework
2. Add ```import iZettle``` to every swift file you want to use the framework
3. Go to your porject's .xcodeproj file and click on General, the move the file called iZettle.framework (downloaded previously) to Embedded binaries 



## Create products

There are different ways to create products based on the options you want to include.
The most basic way to create a product is the following:

``` swift
// create chips product with name and price
let chips = Product(id: "chips", price: 5)
```

If you want to set a discount price when the product is purchased as a complement in a menu, you can create the product like this:

``` swift
let chips = Product(id: "chips", price: 5, priceWithMenu:4)
```

Finally, if you want a product to be have the option to add more complements (be a menu), you can use this: 
``` swift
let burger = Product(id: "Burger", price: 9.5, hasModifiers:true)
```

You can also combine all the previous mentioned options:
``` swift
let pasta = Product(id: "pasta", price: 10.5, priceWithMenu:7, hasModifiers:true) 
```
---
#### Example
This is an example of how you could create the products:  

1.Add this inside viewDidLoad()


``` swift
// Only create products the first time the app is loaded
        if (iZettle.productsCreated == false) {
            createProducts() // function that creates the products
            iZettle.productsCreated = true
        }
        
``` 

2.Copy this function to create the products

``` swift
// MARK: - Create products
    func createProducts() {
        // Set up desired currency
        iZettle.currency = "£"
        
        // Create the products of our app with the framework
        let chips = Product(id: "chips", price: 5, priceWithMenu:4)
        let wedges = Product(id: "Potato wedges", price: 5.0,priceWithMenu:4)
        let sweet = Product(id: "Ice cream", price: 4.5,priceWithMenu:4)
        let juice = Product(id: "Juice", price: 2.5,priceWithMenu:2)
        let nuggets = Product(id: "Nuggets", price: 2.8,priceWithMenu:2)
        let water = Product(id: "Water", price: 1.5,priceWithMenu:1)
        let soda = Product(id: "Soda", price: 1.8,priceWithMenu:1)
        let burger = Product(id: "Burger", price: 9.5, hasModifiers:true)
        
        // Add the products to an array in the framework to ease the access to the products
        iZettle.productListArray = [nuggets, sweet, soda, chips, wedges, water, juice, burger]
        
}

``` 

### Values
- ID: the product name
- Price: the product price
- PriceWithMenu: the price of the product when it's can be added as a complement in the menu. For example, fries can cost 5 pounds,
but if purchased with a menu the price is reduced to 4 pounds. If you don't want to use this optin just add the same price as the previous 'price'
parameter. Default value is Price.
- hasModifiers: set it to 'false' if the product does not have any complement. Set it to 'true' if you want the option to choose a complement
(e.g. fries) for the product (e.g. burger). Default value is False.


## Checkout: add products to the cart
You can easily add products to the cart:
``` swift
// Add the selected product to the cart only if it hasn't been added before (i.e. quantity is 0)
            if (soda.quantity < 1) {
                checkout.cartList.append(soda)
            }
            
            // Increase the quantity of the selected product
            soda.quantity += 1
``` 
#### Example
As an example, you could create a column of products, each with different sender tag. Once a products is selected the following function is called:


``` swift
 @IBAction func productSelection(_ sender: UIButton) {
 
        // If the product has modifiers, go to modifiers selection page before adding the product to the cart
        if (iZettle.productListArray[sender.tag].hasModifiers) {
            performSegue(withIdentifier: "goToModifers", sender: nil)
        }
        
        // if the product doesn't have modifiers, add it to the cart
        else {
            // Add it only if it hasn't been added before (i.e. quantity is 0)
            if (iZettle.productListArray[sender.tag].quantity < 1) {
                checkout.cartList.append(iZettle.productListArray[sender.tag])
            }
            
            // Increase the quantity of the product
            iZettle.productListArray[sender.tag].quantity += 1
            
            print (iZettle.productListArray[sender.tag].quantity) //  quantity is increased by one
            
            performSegue(withIdentifier: "goToBill", sender: nil) // move to bill
        }
        
    }
``` 

### (Optional) Add a menu to the cart
In the case you want to create menus (groups of products with a discount price), then you have to use the parameters ``` isModifier```  and ```priceWithMenu```.
First, create the products that will be included as a complement in the menu:

``` swift
let soda = Product(id: "Soda", price: 1.8,priceWithMenu:1)
let water = Product(id: "Water", price: 1.5,priceWithMenu:1)
```
Now, create the main product that can include complements: 

``` swift
let burger = Product(id: "Burger", price: 9.5, hasModifiers:true)
``` 

Finally, add the menu to the cart with the following code:

``` swift
// Add the menu
checkout.addMenuToCart(mainProduct: burger, modifierName: [soda.id!, water.id!], optionName: "Ketchup")

``` 

The ```optionName``` used before is any complement of the menu that doesn't add any price. For example, choosing fries (a modifier) 
to the menu increases the price, but adding ketchup (an option), doesn't change the price. ```optionName``` is just a string
that will be added to the menu name to indicate that option has been selected. If you don't to add any options, just use:

``` swift
// Add the menu
checkout.addMenuToCart(mainProduct: burger, modifierName: [soda.id!, water.id!], optionName: "")
``` 

## Payment
You can iterate thorugh the checkout products as following:
``` swift
for product in checkout.cartList {
            print (product.id)  // print product name
            print (product.quantity)  // print product quantiy
            print (product.price * Double(product.quantity)) // print total price for a given product
        }
``` 

To get the final price for all the prodcuts on the cart, just call the function ```  checkout.invoice() ```:
``` swift
        print ("Total Price:")
        print (checkout.invoice())
``` 
After making the payment, you can remove the products from the cart with this:
``` swift
// All paid, so set all product quantities to 0
            for product in checkout.cartList {
                product.quantity = 0
            }
            checkout.cartList.removeAll()
``` 
