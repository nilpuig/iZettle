//
//  iZettle.swift
//  iZettle
//
//  Created by Haley on 11/6/17.
//  Copyright © 2017 Nil. All rights reserved.
//

import Foundation

public struct iZettle {
    public static var currency: String? = nil
    public static var productList: Dictionary = [String: AnyClass]()
    public static var productListArray: Array = [Product]()
    public static var productsCreated: Bool = false
}
