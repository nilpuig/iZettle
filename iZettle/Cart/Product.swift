//
//  Product.swift
//  iZettle
//
//  Created by Haley on 11/6/17.
//  Copyright © 2017 Nil. All rights reserved.
//

import Foundation

public class Product {
    public var id: String
    public var price: Double
    public var priceWithMenu: Double
    public let hasModifiers: Bool
    public var quantity: Int
    
    public init(id: String, price: Double, priceWithMenu: Double, hasModifiers: Bool) {
        self.id = id
        self.price = price
        self.priceWithMenu = priceWithMenu
        self.hasModifiers = hasModifiers
        self.quantity = 0
    }
    
    public init(id: String, price: Double, priceWithMenu: Double) {
        self.id = id
        self.price = price
        self.priceWithMenu = priceWithMenu
        self.hasModifiers = false
        self.quantity = 0
    }
    
    public init(id: String, price: Double, hasModifiers: Bool) {
        self.id = id
        self.price = price
        self.priceWithMenu = price
        self.hasModifiers = hasModifiers
        self.quantity = 0
    }
    
    public init(id: String, price: Double) {
        self.id = id
        self.price = price
        self.priceWithMenu = price
        self.hasModifiers = false
        self.quantity = 0
    }
}


