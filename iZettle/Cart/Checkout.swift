//
//  File.swift
//  iZettle
//
//  Created by Haley on 11/7/17.
//  Copyright © 2017 Nil. All rights reserved.
//

import Foundation


public struct checkout {
    // cartList contains all the products that are added to the cart
    public static var cartList: Array = [Product]()
    
    // Get the total price to be paid to buy all the products on the cart
    public static func invoice () -> Double {
        var totalPrice = 0.0
        
        if (checkout.cartList.count != 0) {
            for product in checkout.cartList {
                totalPrice += product.price * Double(product.quantity)
            }
        }
        return totalPrice
        
    }
    
    // Add the menu to the cart
    // Parameters info: the main product is, for example, the burger and the modifier is the fries
    public static func addMenuToCart(mainProduct : Product, modifierName: [String], optionName: String) {
        var menuName = "Menu "
        var totalPrice = mainProduct.price

        // Add the price of modifiers to the menu
        for product in iZettle.productListArray {
            for modifier in modifierName {
                if (product.id == modifier ) {
                    menuName += product.id + " "
                    totalPrice += product.priceWithMenu
                }
            }
        }
        
        menuName += optionName
        
        // Check if menu product was created before
        var productCreated = false
        for product in checkout.cartList {
            if (product.id == menuName) {
                productCreated = true
                product.quantity += 1 // Increase the menu quantity
            }
        }
        // it it was not created, initialize it and add to cart
        if (!productCreated) {
            let menu = Product(id: menuName, price: totalPrice)
            checkout.cartList.append(menu)
            menu.quantity += 1 // Increase the menu quantity
        }
    }
}
